def ints(n):
    """generator that yields the next integer"""
    yield n
    yield from ints(n + 1)


def sieve(s):
    """sieve generator that yields the next prime number
    input: s - integer generator"""
    n = ints(s)
    yield n
    yield from sieve(i for i in s if i % n != 0)


def primes(n):
    """returns a list of the first n primes"""
    out = []
    for i in range(n):
        out.append(sieve(ints(2))    
    return [sieve(ints(2)) for i in range(n)]
    